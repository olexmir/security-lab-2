"use strict";

const MersenneTwister = require('mersennetwister');
const MT = require('./mt').MT;

const API = "http://ec2-35-159-11-170.eu-central-1.compute.amazonaws.com/casino";
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const lcgMode = 'Lcg';
const mtMode = 'Mt';
const betterMtMode = 'BetterMt';

/*
* Just use wolfram and three requests ^_^
* integer solutions[ (a * 1707118539  + c) mod 2^32= 726934190; (a * 726934190  + c) mod 2^32 = 1485048373]
*/
const a = 1664525;
const c = 1013904223;
const m = Math.pow(2, 32);
const jackpot = 1000000;

const lcg = (last) => parseInt(parseInt(a * last + c) % m);

const request = (mode, accountId, bet, number) => {
  const url = `${API}/play${mode}?id=${accountId}&bet=${bet}&number=${number}`;
  console.log(url);
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.open('GET', url, false);
  xmlHttp.send(null);
  return JSON.parse(xmlHttp.responseText);
};

const requestCreate = (accountId) => {
  const url = `${API}/createacc?id=${accountId}`;
  console.log(url);
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.open('GET', url, false);
  xmlHttp.send(null);
  return JSON.parse(xmlHttp.responseText);
};


const requestAsync = (mode, accountId, bet, number, callback) => {
  const url = `${API}/play${mode}?id=${accountId}&bet=${bet}&number=${number}`;
  console.log(url);
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = () => {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
      callback(xmlHttp.responseText);
    }
  };
  xmlHttp.open('GET', url, true);
  xmlHttp.send(null);
};

const task1 = (accountID) => {
  const response = request(lcgMode, accountID, 1, 1);
  console.log('First response', response);
  let lastNumber = response.realNumber;
  const money = parseInt(response.account.money);
  while (money <= jackpot) {
    const number = lcg(lastNumber);
    const response = request(lcgMode, accountID, money, number);
    const newMoney = response.account && response.account.money;
    if (!newMoney) {
      task1(accountID);
      break;
    }
    console.log('Response', response);
    lastNumber = response.realNumber;
  }
};

requestCreate('oleksmir-101');
task1('oleksmir-101');


const task2 = (accountID) => {
  // const response = request(mtMode, accountID, 1, 1);

  const url = `https://us-central1-oleksmir-test.cloudfunctions.net/mt`;
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.open('GET', url, false);
  xmlHttp.send(null);
  const response = JSON.parse(xmlHttp.responseText);

  console.log('Response', response);
  // console.log('Money:', response.account.money);
  const realNumber = response.realNumber;

  let isSeedFined = false;

  const generator = new MT();

  const seed = parseInt(new Date().getTime() / 1000);

  for (let offset = 0; offset <= 100000; offset++) {
    generator.init_genrand(seed - offset);
    const temp = generator.genrand_int32();
    console.log(`{${temp} - ${realNumber}}`);
    if (temp === realNumber) {
      isSeedFined = true;
      break;
    }
  }

  if (!isSeedFined) {
    console.log('Cannot find seed');
  } else {
    console.log('Sid was found!');
  }

  return;
  // while (money <= jackpot) {
  //   const number = generator.random_int();
  //   const response = request(lcgMode, accountID, money, number);
  //   console.log('Response', response);
  // }
};

// requestCreate('v91123');
// task2('v91');

const task3 = (accountID) => {

  function unBitshiftLeftXor(value, shift) {
    let i = 0;
    let result = 0;
    while (i * shift < 32) {
      let partMask = (-1 << (32 - shift)) >>> (shift * i);
      let part = value & partMask;
      value ^= part >>> shift;
      result |= part;
      i++;
    }
    return result;
  }

  function unBitshiftLeftXor(value, shift, mask) {
    let i = 0;
    let result = 0;
    while (i * shift < 32) {
      let partMask = (-1 >>> (32 - shift)) << (shift * i);
      let part = value & partMask;
      value ^= (part << shift) & mask;
      result |= part;
      i++;
    }
    return result;
  }

  let state = new Array(624);
  function hack() {
    for (let i = 0; i < 624; ++i) {
      const response = request(betterMtMode, accountID, 1, 1);
      console.log(i + 1);
      let number = response.realNumber;
      number = unBitshiftLeftXor(number, 18);
      number = unBitshiftLeftXor(number, 15, 0xEFC60000);
      number = unBitshiftLeftXor(number, 7, 0x9D2C5680);
      number = unBitshiftLeftXor(number, 11);
      state[i] = number;
    }
  }

  hack();
  const generator = new MersenneTwister();
  generator.seedArray(state);

  let money = 0;

  while (money <= jackpot) {
    const number = generator.int();
    const response = request(betterMtMode, accountID, 2, number);
    console.log('Response', response);
    // money = response.account.money;
  }

};
//
// requestCreate('v12');
// task3('v12');